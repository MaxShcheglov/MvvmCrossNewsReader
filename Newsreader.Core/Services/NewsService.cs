﻿using System.Collections.Generic;
using Newsreader.Core.Models;
using MvvmCross.Platform.Platform;

namespace Newsreader.Core.Services
{
	public class NewsService : INewsService
	{
		private readonly IHttpClientService _httpClientService;
		private readonly IMvxJsonConverter _serializer;
		private const string hostAddress = @"https://api.tinkoff.ru/v1/news";

		public NewsService(IHttpClientService httpClientService,
						   IMvxJsonConverter serializer)
		{
			_httpClientService = httpClientService;
			_serializer = serializer;
		}

		public List<FeedPayload> GetNewsFeed()
		{
			var json = _httpClientService.GetResponseContent(hostAddress);
			var deserialized = _serializer.DeserializeObject<FeedRootObject>(json).Payload;

			return deserialized;
		}

		public ArticlePayload GetArticleRootObject(int id)
		{
			var json = _httpClientService.GetResponseContent(BuildHostAddressWithId(id));
			var deserialized = _serializer.DeserializeObject<ArticleRootObject>(json).Payload;

			return deserialized;
		}

		private string BuildHostAddressWithId(int id) => hostAddress.Replace("news", $"news_content?id={id}");
	}
}
