﻿namespace Newsreader.Core.Services
{
	public interface IHttpClientService
	{
		string GetResponseContent(string url);
	}
}