﻿using System;
using System.Net.Http;

namespace Newsreader.Core.Services
{
	public class HttpClientService : IHttpClientService
	{
		private const int RequestTimeoutInSeconds = 10;

		public string GetResponseContent(string url)
		{
			using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromSeconds(RequestTimeoutInSeconds) })
			{
				var response = httpClient.GetAsync(url).Result;
				var responseContent = response.Content.ReadAsStringAsync().Result;

				// by calling .Result you are synchronously reading the result
				return responseContent;
			}
		}
	}
}


