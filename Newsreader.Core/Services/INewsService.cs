﻿using Newsreader.Core.Models;
using System.Collections.Generic;

namespace Newsreader.Core.Services
{
	public interface INewsService
	{
		List<FeedPayload> GetNewsFeed();
		ArticlePayload GetArticleRootObject(int id);
	}
}
