﻿namespace Newsreader.Core.Services
{
	public interface IHtmlDecodeService
	{
		string HtmlDecode(string input);
	}
}