﻿using HtmlAgilityPack;
using System.Net;

namespace Newsreader.Core.Services
{
	public class HtmlDecodeService : IHtmlDecodeService
	{
		private HtmlDocument _document = new HtmlDocument();

		public string HtmlDecode(string input)
		{
			_document.LoadHtml(input);

			var innerText = _document.DocumentNode.InnerText;
			var decodedText = WebUtility.HtmlDecode(innerText);

			return decodedText;
		}
	}
}
