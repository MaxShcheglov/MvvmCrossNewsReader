﻿namespace Newsreader.Core.Models
{
	public class ArticleRootObject
	{
		public string ResultCode { get; set; }
		public ArticlePayload Payload { get; set; }
		public string TrackingId { get; set; }
	}
}
