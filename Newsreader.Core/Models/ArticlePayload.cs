﻿namespace Newsreader.Core.Models
{
	public class ArticlePayload
	{
		public Title Title { get; set; }
		public CreationDate CreationDate { get; set; }
		public LastModificationDate LastModificationDate { get; set; }
		public string Content { get; set; }
		public int BankInfoTypeId { get; set; }
		public string TypeId { get; set; }
	}
}
