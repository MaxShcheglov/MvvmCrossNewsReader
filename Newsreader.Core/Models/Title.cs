﻿namespace Newsreader.Core.Models
{
	public class Title
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Text { get; set; }
		public PublicationDate PublicationDate { get; set; }
		public int BankInfoTypeId { get; set; }
	}
}
