﻿namespace Newsreader.Core.Models
{
	public class CreationDate
	{
		public long Milliseconds { get; set; }
	}
}
