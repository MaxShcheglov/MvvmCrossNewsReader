﻿namespace Newsreader.Core.Models
{
	public class LastModificationDate
	{
		public long Milliseconds { get; set; }
	}
}
