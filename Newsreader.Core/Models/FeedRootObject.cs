﻿using System.Collections.Generic;

namespace Newsreader.Core.Models
{
	public class FeedRootObject
	{
		public string ResultCode { get; set; }
		public List<FeedPayload> Payload { get; set; }
		public string TrackingId { get; set; }
	}
}
