﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using MvvmCross.Platform.Platform;
using MvvmCross.Plugins.Json;
using Newsreader.Core.ViewModels;

namespace Newsreader.Core
{
	public class App : MvxApplication
	{
		public override void Initialize()
		{
			CreatableTypes()
				.EndingWith("Service")
				.AsInterfaces()
				.RegisterAsLazySingleton();

			Mvx.RegisterType<IMvxJsonConverter, MvxJsonConverter>();

			RegisterNavigationServiceAppStart<MainMenuViewModel>();
		}
	}
}


