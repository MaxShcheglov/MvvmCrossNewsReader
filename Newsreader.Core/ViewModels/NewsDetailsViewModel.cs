﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using Newsreader.Core.Navigation;
using Newsreader.Core.Services;
using Newsreader.Core.ViewModels;

[assembly: MvxNavigation(typeof(NewsDetailsViewModel), nameof(NewsDetailsViewModel))]

namespace Newsreader.Core.ViewModels
{
	public class NewsDetailsViewModel : MvxViewModel<NewsDetailsNavigationParameters>
	{
		private readonly INewsService _newsService;
		private readonly IHtmlDecodeService _htmlDecodeService;
		private string _title;
		private string _content;

		public NewsDetailsViewModel(INewsService newsService, IHtmlDecodeService htmlDecodeService)
		{
			_newsService = newsService;
			_htmlDecodeService = htmlDecodeService;
		}

		public override void Prepare(NewsDetailsNavigationParameters parameter)
		{
			var article = _newsService.GetArticleRootObject(parameter.Id);

			Title = article.Title.Text;
			Content = _htmlDecodeService.HtmlDecode(article.Content);
		}

		public string Title
		{
			get => _title;
			set
			{
				_title = value;
				RaisePropertyChanged(() => Title);
			}
		}

		public string Content
		{
			get => _content;
			set
			{
				_content = value;
				RaisePropertyChanged(() => Content);
			}
		}
	}
}
