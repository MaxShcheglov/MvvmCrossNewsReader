﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System.Collections.Generic;
using System.Windows.Input;

namespace Newsreader.Core.ViewModels
{
	public class MainMenuViewModel : MvxViewModel
	{
		private IMvxNavigationService _navigationService;

		public MainMenuViewModel(IMvxNavigationService navigationService)
		{
			_navigationService = navigationService;

			MenuItems = new List<MenuItem>
			{
				new MenuItem("News Reader", this, nameof(NewsListViewModel))
			};
		}

		public IMvxNavigationService NavigationService
		{
			get => _navigationService;
			set => _navigationService = value;
		}

		public List<MenuItem> MenuItems { get; private set; }

		public class MenuItem
		{
			public MenuItem(string title, MainMenuViewModel parent, string viewModelUrl)
			{
				Title = title;
				ShowCommand = new MvxCommand(() => parent.NavigationService.Navigate(viewModelUrl));
			}

			public string Title { get; private set; }
			public ICommand ShowCommand { get; private set; }
		}
	}
}
