﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using Newsreader.Core.Models;
using Newsreader.Core.Navigation;
using Newsreader.Core.Services;
using Newsreader.Core.ViewModels;
using System.Collections.Generic;
using System.Linq;

[assembly: MvxNavigation(typeof(NewsListViewModel), nameof(NewsListViewModel))]

namespace Newsreader.Core.ViewModels
{
	public class NewsListViewModel : MvxViewModel
	{
		private readonly IMvxNavigationService _navigationService;
		private readonly INewsService _newsService;
		private List<FeedPayload> _feedPayloads;

		public NewsListViewModel(INewsService newsService, IMvxNavigationService navigationService)
		{
			_navigationService = navigationService;
			_newsService = newsService;

			FeedPayloads = PrepareFeedPayloads();
		}

		private List<FeedPayload> PrepareFeedPayloads()
		{
			var sourceFeedPayloads = _newsService.GetNewsFeed();

			sourceFeedPayloads = sourceFeedPayloads.Select(
				payload =>
				{
					payload.ShowCommand = new MvxCommand(
						() => _navigationService.Navigate<NewsDetailsViewModel, NewsDetailsNavigationParameters>(
							new NewsDetailsNavigationParameters() { Id = int.Parse(payload.Id) }
						)
					);
					return payload;
				}
			).ToList();
			
			return sourceFeedPayloads;
		}

		public List<FeedPayload> FeedPayloads
		{
			get => _feedPayloads;
			set
			{
				_feedPayloads = value;
				RaisePropertyChanged(() => FeedPayloads);
			}
		}
	}
}
