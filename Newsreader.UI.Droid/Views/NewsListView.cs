﻿using Android.App;
using Android.Content.PM;
using MvvmCross.Droid.Views;

namespace Newsreader.UI.Droid.Views
{
	[Activity(Label = "News List", ScreenOrientation = ScreenOrientation.Portrait)]
	public class NewsListView : MvxActivity
	{
		protected override void OnViewModelSet()
		{
			SetContentView(Resource.Layout.Page_NewsListView);
		}
	}
}