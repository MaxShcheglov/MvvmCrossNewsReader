﻿using Android.App;
using Android.Content.PM;
using MvvmCross.Droid.Views;

namespace Newsreader.UI.Droid.Views
{
	[Activity(Label = "News Details", ScreenOrientation = ScreenOrientation.Portrait)]
	public class NewsDetailsView : MvxActivity
	{
		protected override void OnViewModelSet()
		{
			SetContentView(Resource.Layout.Page_NewsDetailsView);
		}
	}
}