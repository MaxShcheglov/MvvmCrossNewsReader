﻿using Android.App;
using MvvmCross.Droid.Views;

namespace Newsreader.UI.Droid
{
	[Activity(Label = "News Reader", MainLauncher = true, NoHistory = true, Icon = "@drawable/icon")]
	public class SplashScreenActivity : MvxSplashScreenActivity
	{
		public SplashScreenActivity()
			: base(Resource.Layout.SplashScreen)
		{
		}
	}
}